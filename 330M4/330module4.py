# -*- coding: utf-8 -*-
"""
Created on Thu Oct 02 22:29:20 2014

@author: Diresal
"""
import re;
import sys, os
 
if len(sys.argv) < 2:
    sys.exit("Usage: %s filename" % sys.argv[0])
filename = sys.argv[1]
if not os.path.exists(filename):
    sys.exit("Error: File '%s' not found" % sys.argv[1])
#print "======="
#print 'not none',None
#a=None;
#print a,not(a)
#import fileinput;
stars = {}
file= open(filename);
#refile=re.compile(file);
#2line = file.readline()
lines = file.readlines()
for line in lines:
    #print line;
    if line.startswith('#'):
        #2line = file.readline()
        continue;
    else:
        if line.startswith('='):
            #2line = file.readline()
            continue;
        else:            
                string=line;
                #print string
                #print "find name"
                rename = re.search("(\w+\s\w+)",string);
                #print rename
                if  rename:
                    #print rename
                    name = rename.group()
                    if name not in stars.keys():
                        stars[name]=[0,0,0,0]
                    #print "find batted time"
                    rebatted = re.search("batted \d{1,3}",string);
                    battednum = re.search("\d",rebatted.group());
                    batted = float(battednum.group())
                    stars[name][0]=stars[name][0]+batted
                    #print "find hits"
                    rehits = re.search("\d\shits",string)
                    hitsnum = re.search("\d",rehits.group());
                    hits = float(hitsnum.group())
                    stars[name][1]=stars[name][1]+hits
                    #print "find runs"
                    reruns = re.search("\d\sruns",string)
                    runsnum = re.search("\d",reruns.group());
                    runs = float(runsnum.group())
                    stars[name][2]=stars[name][2]+runs
                    # A player's batting average is that player's total hits divided 
                    #by that player's total at-bats throughout the entire season.
                    #2line = file.readline()
for name in stars:
    stars[name][3]=stars[name][1]/stars[name][0]                    
#print stars
sorted_stars = sorted(stars.iteritems(),key=lambda stars:stars[1][3],reverse=True)
for name in sorted_stars:
    #print name
    #print name[0],name[1][3]
    print "%s : %4.3f" %(name[0],name[1][3])
    #print "%s : %10.3f" %(name.keys(),name.values()[3])
file.close()

